# Tests avec JUnit 5 notes
- Le TDD permet de créer un nouveau test automatique avant d'écrire du code
- Le TDD permet de communiquer sur ce que vous testez afin de détecter les bug en codant
## AssertJ
Permet de rendre les tests suffisamment significatifs pour éviter les erreurs.
Frameworks similaires Hamcrest dans JUnit 4; Truth et AssertJ

### Avantage d'AssertJ

- Créer un code lisible en créant des assertions proches du langage naturel.
- Simple à utiliser pour les développeurs

## Couverture de tests et qualité du code

TDD: rouge-vert-refactor

Un code est **couvert** par les tests s'il est exécuter par au moins un test.
 - Les différentes façons de mesurer la quantité de code
    * le nombre de lignes de code;
    * Le nombre d'instructions;
    * Le nombre de branches: le nombre de zones de code par rapport à des conditions. Un ensemble d'instructions if/else génère deux zones de code, que l'on appelle aussi branches.
    * Le nombre de fonctions/méthodes 
 
 Pour obtenir des rapports de couverture de test, on peut utiliser le plugin jacoco-maven.
 
 - SonarCloud pour la qualité du code intégrable avec GitHub et gratuit avec un projet open source
 - L'audit de sécurité des dépendances: OWASP Dependency checker qui scanne les vulnérabilités de sécurité. **Checkstyle** permet de vérifier les dénominations de variables et de classes.
 
 ## Etiquetage des tests avec des annotations JUnit
 
 - **@tag** permet d'étiquetter un test ou catégoriser les tests.
 - **@extendwith** permet de personnaliser le fonctionnement interne de JUnit.
 - **@disabled** permet de désactiver d'un test.
 - **@DisplayName** permet de nommer vos tests de façon lisible.
 - **@Nested** permet de regrouper les tests dans une classe interne. *Remarque: * si un test échoue, tout le groupe désigné par cette annotation échoue.
 
 ## La méthode FIRST
 - **F** pour *fast** : Rapide, toujours faire abstraction en utilisant les mocks en cas de lecture de fichiers ou connexion sur un réseau.
 - **I** pour *Isolated*: Isolé et indépendant (Un problème - une cause du problème - une solution). Eviter les effets de bord
 - **R** pour *Repeatable*: Répétable, peu importe combien fois le test est exécuter, on devra avoir toujours la même réponse.
 - **S** pour *Self-validating*: Autovalidation (les tests ne laisse aucun doute sur leur succès ou leur échec)
 - **T** pour *Thorough*: approfondi, le code est testé largement pour des cas négatifs et positifs
 
 ### Nommage des tests unitaires
 
 Mon nommage à moi: givenStateUnderTestWhenMethodeActionThenExpectedBehavior
 
 Exple: givenTwoPositiveIntegerWhenAddedThenShouldBeSummed()
 
 Communiquer clairement sur ce sur quoi portent les tests.
 
 ## Amélioration des tests
 
 ### Les cas limites ou edges cases
 
 - Les cas limites dus aux règles métiers
 - Les cas limites dus aux limitations techniques et physiques
 - Les cas pathologiques ou corner cases:ont lieu lorsque plusieurs cas limites se présentent ou se mélangent avec des dysfonctionnements extérieurs.
 exple de cas pathologiques:
    - Que se passent-ils avec les données utilisateurs si le programme crashe?
    - Que se passe-t-il si mon utilisateur fait deux fois la même chose?