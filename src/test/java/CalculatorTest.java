import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.time.Instant;
import java.util.Set;


import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
@DisplayName("Test du Calculator")
@Nested
@ExtendWith(LoggingExtension.class)
public class CalculatorTest {

    private Calculator calculator;

    private static Instant instant;
    private  Logger logger;

    public void setLogger(Logger logger){
        this.logger = logger;
    }

    @BeforeEach
    public void initCalculator(){
        logger.info("Appel avant chaque test.");
        calculator = new Calculator();
    }
    @AfterEach //appel après chaque test
    public void underCalculator(){
        logger.info("Appel apres chaque test.");
        calculator = null;
    }
    @BeforeAll // appel avant tous les cas de tests
    static void initStartingTime (){
       java.util.logging.Logger.getLogger(CalculatorTest.class.getName()).info("Appel pour mesurer le temps avant tous les tests.");
        instant = Instant.now();
    }
    @AfterAll // Appel après l'exécution de tous les cas de tests
    public static void showTestDuration(){
        java.util.logging.Logger.getLogger(CalculatorTest.class.getName()).info("Appel apres tous les tests pour connaitre le temps d'execution des tests");
        Instant endedAt = Instant.now();
        long duration = Duration.between(instant,endedAt).toMillis();
        java.util.logging.Logger.getLogger(CalculatorTest.class.getName()).info("Duree des tests: "+duration);

    }
    @Test
    @Tag("Operattionbase")
    @DisplayName("Addition de 2 nombres entiers")

    public void addTest(){
        //ARRANGE -> Organiser
        final int a = 4;
        final int b = 5;

        //ACT -> Action
        final int somme = calculator.add(a,b);

        // ASSERT -> Verifier
        assertThat(somme).isEqualTo(9);
        //assertEquals(9,somme);
    }
    @Disabled("Plus besoin de faire des multiplication")
    @Test
    @Tag("Operattionbase")
    @DisplayName("Multiplication de 2 nombres entiers")
    void multiplyTest(){
        //ARRANGE
        final int a = 4;
        final int b = 5;

        // ACT
        final int produit = calculator.multiply(a,b);
        // ASSERT
        assertThat(produit).isEqualTo(20);
        //assertEquals(20,produit);
    }

    //passage des parametres à la methode en faisant un passage par valeur
    @ParameterizedTest(name = "{0} x 0 doit etre egal a 0")
    @ValueSource(ints = {1,12,45,89,1235})
    @DisplayName("Mutiplication de nombres par zéro")
    public void multiplyToZeroTest(int arg){

        //ARRANGE -> tout est pret

        // ACT -> multiplier par 0
        int result = calculator.multiply(arg,0);

        // ASSERT -> ca vaut toujours 0
        assertThat(result).isEqualTo(0);
        assertEquals(0, result);

    }
    //passage des parametres en faisant un passage par csv source: valeur et le resultat attendu
    @ParameterizedTest(name = "{0} + {1} should equal to {2}")
    @CsvSource({ "1,1,2", "2,3,5", "42,57,99" })
    @DisplayName("Additon de nombres à partir d'une source csv")
    public void add_shouldReturnTheSum_ofMultipleIntegers(int arg1, int arg2, int expectResult) {
        // Arrange -- Tout est prêt !

        // Act
        int actualResult = calculator.add(arg1, arg2);

        // Assert
        assertThat(expectResult).isEqualTo(actualResult);
        //assertEquals(expectResult, actualResult);
    }
    @Test
    @DisplayName("Rechercher les chiffres uniques qui compose un nombre entier donné")
    public void digitsSet_ofpositiveInteger(){
        //GIVEN
        int number = 95897;

        //WHEN
        Set<Integer> actualDigits = calculator.digitsSet(number);

        //THEN
        assertThat(actualDigits).containsExactlyInAnyOrder(5, 7, 8, 9);
    }
}
